import React from 'react'
import {Shape} from './shape'
import {getIcon} from '../shape'

export const HumanBoard = ({board, game, onHumanPlay}) => {
  return (
    <div className='board'>
      <Title title='HUMAN'/>
      <Shapes disabled={game.player !== 'human'} onHumanPlay={onHumanPlay}/>
      <Message round={game.round} type={'human'}/>
      <Shape board={board}/>
    </div>
  )
}

export const ComputerBoard = ({board, game}) => {
  return (
    <div className='board'>
      <Title title='COMPUTER'/>
      <div className='shapes'/>
      <Message round={game.round} type={'computer'}/>
      <Shape board={board}/>
    </div>
  )
}

const getMessage = (round, type) => {
  if(round.winner === 'tie') return ['TIE!', 'orange']
  else if(round.winner === type) return ['WIN!', 'green']
  else return []
}

const Message = ({round, type}) => {
  const [message, color] = getMessage(round, type)
  if(!message)return <div className='message'/>
  const style = {color}
  return (
    <div style={style} className='message'>
      <span>{message}</span>
    </div>
  )
}

const Title = ({title}) => {
  return (
    <div className='title'>
      <span>{title}</span>
    </div>
  )
}

const Shapes = (props) => {
  return (
    <div className='shapes'>
      <ShapeButton {...props} name={'rock'} color='#3E50B4'/>
      <ShapeButton {...props} name={'paper'} color='#2095F2'/>
      <ShapeButton {...props} name={'scissors'} color='#00BBD3'/>
    </div>
  )
}

const ShapeButton = ({name, color, onHumanPlay, disabled}) => {
  if(disabled){
    return (
      <div className='button disabled'>
        <i className={`fa fa-${getIcon(name)}`}/> 
      </div>
    )
  }else{
    const style={ color }
    const handleClick = (e) => {
      e.stopPropagation()
      onHumanPlay(name, color)
    }
    return (
      <a href='#' style={style} className='button' onClick={handleClick}>
        <i className={`fa fa-${getIcon(name)}`}/> 
      </a>
    )
  }
}
