import React from 'react'
import '../../public/styles/app.css'
import {HumanBoard, ComputerBoard} from './boards'
import {GameInfo} from './info'
import {Restart} from './widgets'

export const Roshambo = ({human, computer, game, history, onHumanPlay}) => {
  return (
    <div className='roshambo'>
      <HumanBoard board={human} game={game} onHumanPlay={onHumanPlay}/>
      <GameInfo game={game} history={history}/>
      <ComputerBoard game={game} board={computer}/>
    </div>
  )
}
