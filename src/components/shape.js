import React from 'react'
import {getIcon} from '../shape'

export const Shape = ({board}) => {
  if(!board)return <div/>
  if(board.isRolling)
    return (
      <div style={style} className='shape'>
        <i className={`fa fa-spinner fa-spin`}/> 
      </div>
    )
  if(!board.shape)return <div/>
  const style = { color: board.color}
  return (
    <div style={style} className='shape'>
      <i className={`fa fa-${getIcon(board.shape)}`}/> 
    </div>
  )
}

