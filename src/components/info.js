import React from 'react'
import {Shape} from './shape'

export const GameInfo = ({game, history}) => {
  return (
    <div className='info'>
      <Scores score={game.score}/>
      <Rounds value={game.round.value}/>
      <History history={history}/>
    </div>
  )
}

const Scores = ({score}) => {
  return (
    <div className='scores'>
      <Score title={'Wins'} value={score.wins.human}/>
      <Score title={'Ties'} value={score.ties}/>
      <Score title={'Wins'} value={score.wins.computer}/>
    </div>
  )
}

const Rounds = ({value}) => {
  const msg = value ? `Rounds ${value}` : "Let's start first round"
  return (
    <div className='rounds'>
        <span>{msg}</span>
    </div>
  )
}

const History = ({history=[]}) => {
  const rounds = history.slice(history.length > 5 ? history.length-5 : 0, history.length).reverse().map( r => <PastRound key={r.round} round={r}/> )
  return (
    <div className='history'>
      {rounds}
    </div>
  )
}

const Score = ({title, value}) => {
  return (
    <div className='score'>
      <div className='title'>
        <span>{title}</span>
      </div>
      <div className='value'>
        <span>{value}</span>
      </div>
    </div>
  )
}

const getColor = (who, winner) => {
  if(winner === 'tie') return 'orange'
  return winner === who ? 'green' : 'red'
}

const PastRound = ({round}) => {
  return (
    <div className='pastround'>
      <Shape board={{shape: round.shapes.human, color: getColor('human', round.winner)}}/>
      <div className='round'>
        <span>{`Round ${round.round}`}</span>
      </div>
      <Shape board={{shape: round.shapes.computer, color: getColor('computer', round.winner)}}/>
    </div>
  )
}
