export const getIcon = (name) => {
  return {
    paper: 'hand-paper-o',
    rock: 'hand-rock-o',
    scissors: 'hand-scissors-o',
    spinner: 'spinner',
  }[name]  || 'question'
}
