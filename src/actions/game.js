import 'whatwg-fetch';

export const HUMAN_PLAYED = 'HUMAN_PLAYED'
export const COMPUTER_PLAYED = 'COMPUTER_PLAYED'
export const COMPUTER_ROLLING = 'COMPUTER_ROLLING'
export const HUMAN_ROLLING = 'HUMAN_ROLLING'
export const ROUND_STARTED = 'ROUND_STARTED'
export const ROUND_CLOSED = 'ROUND_CLOSED'

export const computerPlay = () => {
  return (dispatch) => {
    dispatch(shapeRolling('computer'))
    fetch('https://hook.io/eric-basley/roshambo').then(res => res.json()).then( ({icon, color}) => {
      dispatch(shapeRolled('computer', icon, color)) 
      dispatch(checkWinner())
    })
  }
}

const shapeRolling = player => {
  return {
    type: player === 'human' ? HUMAN_ROLLING : COMPUTER_ROLLING 
  }
}

const shapeRolled = (player, shape, color) => {
  return {
    type: player === 'human' ? HUMAN_PLAYED : COMPUTER_PLAYED,
    shape,
    color
  }
}

const startRound = () => {
  return {
    type: ROUND_STARTED 
  }
}

const getWinner = ({human, computer}) => {
  if(human.shape === computer.shape ) return 'tie'
  if(human.shape === 'scissors' && computer.shape === 'paper') return 'human'
  if(human.shape === 'paper' && computer.shape === 'rock') return 'human'
  if(human.shape === 'rock' && computer.shape === 'scissors') return 'human'
  return 'computer'
}

const checkWinner = () => {
  return (dispatch, getState) => {
    const state = getState()
    const round =  state.game.round.value
    const shapes = {
      human: state.human.shape,
      computer: state.computer.shape,
    }
    const winner = getWinner({human: state.human, computer: state.computer})
    dispatch({
      type: ROUND_CLOSED,
      round,
      shapes,
      winner
    })
  }
}

export const humanPlay = (shape, color) => {
  return (dispatch) => {
    dispatch(startRound())
    dispatch(shapeRolled('human', shape, color))
    dispatch(computerPlay())
  }
}
