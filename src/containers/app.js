import React, {PropTypes} from 'react'
import {connect} from 'react-redux'
import {Roshambo} from '../components/roshambo'
import {humanPlay} from '../actions/game'

const App = ({dispatch, ...others}) => {
  const handleHumanPlay = (shape, color) =>  dispatch(humanPlay(shape, color))
  return <Roshambo onHumanPlay={handleHumanPlay} {...others}/>
}

App.propTypes = {
  dispatch: PropTypes.func.isRequired
}

const mapStateToProps = (state) => {
  return {
    human: state.human,
    computer: state.computer,
    game: state.game,
    history: state.history,
  }
}

//export default connect(mapStateToProps, null)(App)
export default connect(state => state, null)(App)


