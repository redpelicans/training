import {HUMAN_PLAYED} from '../actions/game'
const initialState = {
  shape: 'question'
}

export default function reducer(state = initialState, action) {
  switch(action.type){
    case HUMAN_PLAYED:
      return {
        shape: action.shape,
        color: action.color,
      }
    default: 
      return state;
  }
}

