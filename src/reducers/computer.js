import {COMPUTER_PLAYED, COMPUTER_ROLLING} from '../actions/game'

export default function reducer(state = {}, action) {
  switch(action.type){
    case COMPUTER_PLAYED:
      return {
        ...state,
        isRolling: false,
        shape: action.shape,
        color: action.color,
      }
    case COMPUTER_ROLLING:
      return {
        ...state,
        isRolling: true
      }
    default: 
      return state;
  }
}

