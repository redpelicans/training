import { combineReducers } from 'redux'

import game from './game'
import human from './human'
import computer from './computer'
import history from './history'

const rootReducer = combineReducers({
  game,
  human,
  computer,
  history,
})

export default rootReducer
