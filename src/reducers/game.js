import {HUMAN_PLAYED, COMPUTER_PLAYED, ROUND_STARTED, ROUND_CLOSED} from '../actions/game'

const initialState = {
  player: 'human',
  round: {
    value: 0,
    winner: undefined
  },
  score:{
    wins: { human: 0, computer: 0},
    ties: 0
  }
}

const scoreReducer = (state, winner) => {
  switch(winner){
    case 'tie':
      return {
        ...state,
       ties:  state.ties + 1
      }
    case 'human':
      return {
        ...state,
       wins:  { ...state.wins, human: state.wins.human + 1}
      }
    case 'computer':
      return {
        ...state,
       wins:  { ...state.wins, computer: state.wins.computer + 1}
      }
  }
}

export default function reducer(state=initialState, action) {
  switch(action.type){
    case ROUND_CLOSED:
      return {
        ...state,
        round: {...state.round, winner: action.winner},
        score: scoreReducer(state.score, action.winner)
      }
    case ROUND_STARTED:
      return {
        ...state,
        round: {value: state.round.value + 1, winner: undefined}
      }
    case HUMAN_PLAYED:
      return {
        ...state,
        player: 'computer'
      }
    case COMPUTER_PLAYED:
      return {
        ...state,
        player: 'human'
      }
    default: 
      return state;
  }
}

