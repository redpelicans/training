import {ROUND_CLOSED} from '../actions/game'

export default function reducer(state = [], action) {
  switch(action.type){
    case ROUND_CLOSED:
      return [ 
        ...state, 
        {
          round: action.round,
          shapes: action.shapes,
          winner: action.winner
        }
      ]
    default: 
      return state;
  }
}

